class DataBase

	def initialize
		@con = Mysql.new $db_host, $db_user, $db_pass, $db_name
	end



	def add_new_client(name,surname,email)
		add_client = @con.query ("INSERT INTO customers (name,surname,email) VALUES('#{name}','#{surname}','#{email}')")
		true
	end

	def add_order(email)
		insert_order = @con.query ("INSERT INTO orders(id_customer) SELECT id FROM customers WHERE email = '#{email}'")
		puts "INSERT INTO orders(id_customer) SELECT id FROM customers WHERE email = '#{email}'"
		puts @con.insert_id
		id = @con.insert_id
	end

	def add_order_item(id_product,quantity,id_order,id_customer)
		insert_order_item = @con.query ("INSERT INTO ordered_items(id_customer,id_item,name,price,quantity,id_order) SELECT '#{id_customer}','#{id_product}',name,price,'#{quantity}','#{id_order}' FROM items WHERE id = '#{id_product}'")
		
	end

	def make_invoice(id_ord)
		insert_invoice = @con.query ("INSERT INTO invoices(id_order) VALUES(#{id_ord})")
		id = @con.insert_id
	end



	def add_external_id_db(contact_external_id_xero,email)
		update_customer = @con.query ("UPDATE customers SET external_id_xero = '#{contact_external_id_xero}' WHERE email = '#{email}' ") 
	end

	def add_id_invoice_to_order(id_invoice,id_ord)
		update_order = @con.query ("UPDATE orders SET id_invoice = #{id_invoice} WHERE id = #{id_ord}")
	end

	def add_external_invoice_id_db(invoice_external_id_xero,id_invoice)
		update_invoice = @con.query ("UPDATE invoices SET external_id_xero = '#{invoice_external_id_xero}' WHERE id = #{id_invoice}")
	end



	def select_items
		select_i = @con.query ("SELECT * FROM items")
	end

	def select_client_email(email)
		select_cn = @con.query ("SELECT * FROM customers WHERE email = '#{email}'")
	end

	def select_orders_without_i
		select_orders_without_i = @con.query ("SELECT orders.id, orders.id_customer FROM orders WHERE orders.id_invoice = 0")
	end

	def select_customer(id_order)
		select_customer_id = @con.query ("SELECT id_customer FROM orders WHERE id = #{id_order} ")

		id_customer = 0
		select_customer_id.each_hash do |row_hash|
			id_customer = row_hash["id_customer"]
		end

		select_customer_row = @con.query ("SELECT * FROM customers WHERE id = #{id_customer}")
		
	end

	def select_data_to_make_invoice(id_invoice)
		select_order = @con.query ("SELECT id_order FROM invoices WHERE id = #{id_invoice} ")
		id_order = 0

		select_order.each_hash do |row_hash|
			id_order = row_hash["id_order"]
		end

		select_from_ordered_items = @con.query ("SELECT name, quantity, price, id_item FROM ordered_items WHERE id_order = #{id_order}")
	end

	def select_invoices
		select_invoices = @con.query ("SELECT * FROM invoices")
		
	end

	def select_id_order_from_invoices(id_invoice)
		select_order = @con.query ("SELECT id_order FROM invoices WHERE id = #{id_invoice} ")
		id_order = 0

		select_order.each_hash do |row_hash|
			id_order = row_hash["id_order"]
		end
		id_order
	end

	def select_ordered_items(id_order)
		select_ordered_items = @con.query ("SELECT * FROM ordered_items WHERE id_order = #{id_order}")
	end
end