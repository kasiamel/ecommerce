class Xero
	def initialize(con_key,con_secret,private_key)
		@client = Xeroizer::PrivateApplication.new(con_key, con_secret, private_key)
	end

	def add_con(name,surname,email)

		contact = @client.Contact.build(:name => "#{email}")
		contact.first_name = "#{name}"
		contact.last_name = "#{surname}"
		contact.email_address = "#{email}"
		contact.save

		contact

	end

	def add_invoice(customer_email,id_invoice,items_hash)

		invoice =  @client.Invoice.build(
		    :type => "ACCREC", 
		    :contact => { :name => "#{customer_email}" },
		    #:date => "2015-10-02",
		    #:due_date => "2015-10-05",
		    :invoice_number => "#{id_invoice}",
		    :total_tax => 20
		  )

		items_hash.each_hash do |row_hash|

		invoice.add_line_item(
		  :description => "#{row_hash["name"]}",
		  :quantity => "#{row_hash["quantity"]}",
		  :unit_amount => "#{row_hash["price"]}",
		  #:id_item => "120"
		)

		end

		invoice.save

		invoice

		
	end

end