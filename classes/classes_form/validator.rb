class Validator

	def initialize(answer,ob_question)
		@answer = answer
		@ob_question = ob_question
	end

	def find_type

		if @ob_question.type == "string"
			string_validation
		elsif @ob_question.type == "email"
			email_validation
		elsif @ob_question.type == "cos"
			puts "to jest type cos"
		end
		 
	end

	def string_validation

		val_answer = @answer.match /^[a-zA-Z ]+$/


		if val_answer != nil 
			return true
		else
			return false
		end

	end

	def email_validation

			val_answer = @answer.match /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*(\.[a-z]+)\z/i


		if val_answer != nil 
			return true
		else
			return false
		end

		
	end
end