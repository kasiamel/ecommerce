class CsvFile

	def initialize(file_name)
		@file_name = file_name
	end

	def make_ob_inoice_item

		item_invoices_from_csv = CSV.read(@file_name, { :col_sep => ":" })

		arr_ob_invoice_item = []

		#quantity,name,nr_article,price
		item_invoices_from_csv.each do |invoice_row|

			if invoice_row.length != 4
				next
			end

			quantity = invoice_row[0]
			name = invoice_row[1]
			nr_article = invoice_row[2]
			price = invoice_row[3]

			ob_inoice_item = InvoiceItem.new(quantity,name,nr_article,price)
			
			arr_ob_invoice_item << ob_inoice_item

		end
		
		arr_ob_invoice_item
	end

	def make_ob_client

		client_from_csv = CSV.read(@file_name, { :col_sep => ":" })
		
		ob_client_personal_data = nil
		#name,address,telephone_number
		client_from_csv.each do |client|

			if client.length != 3
				next
			end 

			name = client[0]
			address = client[1]
			telephone_number = client[2]

			ob_client_personal_data = PersonalData.new(name,address,telephone_number)

		end

		ob_client_personal_data
	end

	def make_ob_dealer

		dealer_from_csv = CSV.read(@file_name, { :col_sep => ":" })

		ob_dealer_personal_data = nil
		#name,address,telephone_number
		dealer_from_csv.each do |dealer|

			if dealer.length != 3
				next
			end 

			name = dealer[0]
			address = dealer[1]
			telephone_number = dealer[2]

			ob_dealer_personal_data = PersonalData.new(name,address,telephone_number)
		end
		
		ob_dealer_personal_data
	end
end