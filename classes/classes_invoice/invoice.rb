class Invoice

	attr_accessor :arr_ob_item, :ob_client, :id_invoice

		@arr_ob_item
		@ob_client
		@id_invoice
	

	def total_amount_with_tax

		total_amount_with_tax = 0
		@arr_ob_item.each do |invoice_item|
			total_w_tax = invoice_item.total_with_tax

			total_amount_with_tax = total_amount_with_tax + total_w_tax
		end

		total_amount_with_tax
		
	end

	def total_amount_without_tax

		total_amount_without_tax = 0
		@arr_ob_item.each do |invoice_item|
			total_wo_tax = invoice_item.total_without_tax
			total_amount_without_tax = total_amount_without_tax + total_wo_tax
		end
		
		total_amount_without_tax
	end
end