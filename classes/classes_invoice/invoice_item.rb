class InvoiceItem

	attr_accessor :quantity, :name, :nr_article, :price

	def initialize(quantity,name,nr_article,price)
		@quantity = quantity.to_f
		@name = name
		@nr_article = nr_article.to_f
		@price = price.to_f
		@tax = 20

	end

	def total_with_tax
		price_with_tax = @quantity * ((@price *  @tax)/100 + @price)
	end

	def total_without_tax
		price_without_tax = @quantity * @price
	end

end