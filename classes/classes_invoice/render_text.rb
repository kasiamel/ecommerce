class RenderText

	#quantity,name,nr_article,price
	def render_invoice(ob_invoice)
		puts "==============================================================================="
		puts "INVOICE NR #{ob_invoice.id_invoice}"
		puts "==============================================================================="
		puts "CUSTOMER DATA"
		puts "NAME:#{ob_invoice.ob_client.name}"
		puts "SURNAME:#{ob_invoice.ob_client.surname}"
		puts "EMAIL:#{ob_invoice.ob_client.email}"
		puts "==============================================================================="
		puts sprintf("%-15s %-15s %-15s %-15s %-15s", "QUANTITY", "NAME", "NR ARTICLE", "PRICE", "TOTAL WITH TAX")

		ob_invoice.arr_ob_item.each do |item|
			#item.quantity
			#item.name
			#item.nr_article
			#item.price
			#item.total_with_tax

			puts sprintf("%-15.2f %-15s %-15.2f %-15.2f %-15.2f", "#{item.quantity}", "#{item.name}", "#{item.nr_article}", "#{item.price}", "#{item.total_with_tax}")
		end

		puts "==============================================================================="
		puts "TOTAL AMOUNT:#{ob_invoice.total_amount_without_tax}"
		puts "TOTAL AMOUNT WITH TAX:#{ob_invoice.total_amount_with_tax}"
	end
end