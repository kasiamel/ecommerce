def action_invoice

	puts "You are in invoices!\n
		  * to view orders without invoice put: v\n
		  * to view orders invoice put: i"

	answer = gets.chomp
	answer = answer.downcase

	if answer == "v"

		ob_data_base  =  DataBase.new
		select_orders_without_i = ob_data_base.select_orders_without_i

		id_orders_in_db = []
		id = nil
		puts sprintf("%-15s %-15s", "ID_ORDER", "ID_CUSTOMER")
		select_orders_without_i.each_hash do |row_hash|
			id = row_hash["id"]
			id_orders_in_db << id
			puts sprintf("%-15.0f %-15.0f", "#{row_hash["id"]}", "#{row_hash["id_customer"]}")

		end

		if id == nil
			puts "Sorry, there is not any orders without inoice."
		else 

			flag = false
			counting = 0
			while flag == false && counting <= 2

				puts "* to make invoice put ID_ORDER(you can choose only one number): \n"
				id_ord = gets.chomp
				id_length = id_ord.length

				include_id = id_orders_in_db.include? id_ord

				if id_length != 0 && include_id == true

					make_invoice = ob_data_base.make_invoice(id_ord)
					id_invoice = make_invoice
					add_id_in_to_ord = ob_data_base.add_id_invoice_to_order(id_invoice,id_ord)

			        select_email_customer = ob_data_base.select_customer(id_ord)

					customer_email = 0
					select_email_customer.each_hash do |row_hash|
						customer_email = row_hash["email"]
					end

					data_to_xero = ob_data_base.select_data_to_make_invoice(id_invoice)

					ob_xero = Xero.new($con_key,$con_secret,$private_key)
					make_invoice_in_xero = ob_xero.add_invoice(customer_email,id_invoice,data_to_xero)
					invoice_external_id_xero = make_invoice_in_xero["invoice_id"]
					add_invoice_external_id_db = ob_data_base.add_external_invoice_id_db(invoice_external_id_xero,id_invoice)
					flag = true

					puts "Thank you! New invoice NR/ID_INVOICE: #{id_invoice} is in data base. "
				else 
					puts "Something went wrong."
					flag = false
					counting = counting + 1
				end
			end
		end


	elsif answer == "i"

		ob_data_base = DataBase.new
		select_invoices = ob_data_base.select_invoices

		id_invoices_in_db = []
		id = nil
		puts sprintf("%-15s %-15s", "ID_INVOICE", "ID_ORDER")
		select_invoices.each_hash do |row_hash|
			id = row_hash["id"]
			id_invoices_in_db << id
			puts sprintf("%-15.0f %-15.0f", "#{row_hash["id"]}", "#{row_hash["id_order"]}")
		end


		if id == nil
			puts "Sorry, there is not any invoices to show."
		else 


			flag = false
			counting = 0
			while flag == false && counting <= 2

				puts "* to view whole invoice put ID_INVOICE(you can choose only one number): \n"
				id_invoice = gets.chomp
				id_length = id_invoice.length

				include_id = id_invoices_in_db.include? id_invoice
				
				if id_length != 0 && include_id == true

					select_items_to_make_invoice = ob_data_base.select_data_to_make_invoice(id_invoice)
					arr_ob_invoice_item = []

					select_items_to_make_invoice.each_hash do |row_hash|
						quantity = row_hash["quantity"]
						name = row_hash["name"]
						nr_article = row_hash["id_item"]
						price = row_hash["price"]

						ob_inoice_item = InvoiceItem.new(quantity,name,nr_article,price)
						arr_ob_invoice_item << ob_inoice_item
					end

					select_id_order_from_invoices = ob_data_base.select_id_order_from_invoices(id_invoice)
					id_order = select_id_order_from_invoices

					select_customer = ob_data_base.select_customer(id_order)

					name = nil
					surname = nil
					email = nil
					select_customer.each_hash do |row_hash|
						name = row_hash["name"]
						surname = row_hash["surname"]
						email = row_hash["email"]
					end

					ob_client = PersonalData.new(name,surname,email)

					ob_invoice = Invoice.new
					ob_invoice.arr_ob_item = arr_ob_invoice_item
					ob_invoice.ob_client = ob_client
					ob_invoice.id_invoice = id_invoice

					render_invoice_text = RenderText.new
					render_invoice_text.render_invoice(ob_invoice)

					flag = true
				else
					puts "Something went wrong."
					flag = false
					counting = counting + 1
				end
			end
		end

	else
		"HELP!"
	end
 
end