require 'rubygems'
require 'xeroizer'
require 'mysql'

require_relative 'config'

require_relative 'actions/action_order'
require_relative 'actions/action_invoice'

require_relative 'classes/xero'
require_relative 'classes/data_base'


require_relative 'classes/classes_form/form'
require_relative 'classes/classes_form/question'
require_relative 'classes/classes_form/question_name'
require_relative 'classes/classes_form/question_surname'
require_relative 'classes/classes_form/question_email'
require_relative 'classes/classes_form/validator'
require_relative 'classes/classes_form/contact_form'
require_relative 'classes/classes_form/email_form'


require_relative 'classes/classes_invoice/invoice'
require_relative 'classes/classes_invoice/invoice_item'
require_relative 'classes/classes_invoice/personal_data'
require_relative 'classes/classes_invoice/render_html'
require_relative 'classes/classes_invoice/render_text'


action = nil
#main loop
while action != "end"

	puts "Hello!\n
			* to make a order put: o\n
			* to search/make a invoice put: i\n
			* to end put: end"

	action = gets.chomp
	action = action.downcase

	if action == "o" 

		action_order

	elsif action == "i"

		action_invoice

	else
		"HELP! Please start program again."
	end
end
	










	


















